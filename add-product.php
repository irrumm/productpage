<?php

require_once "Services/TypeDAL.php";
require_once "Services/ProductDAL.php";
require_once "Product/Book.php";
require_once "Product/Furniture.php";
require_once "Product/DVD.php";

if (!empty($_POST)) {
    $SKU = $_POST['sku'];
    $name = $_POST['name'];
    $price = $_POST['price'];
    $productId = $_POST['productType'];

    $type = getTypeById($productId);

    $property = $_POST['weight'] ?: $_POST['size'] ?: $_POST['height'].'x'.$_POST['width'].'x'.$_POST['length'];
    $product = new $type['typeName']($SKU, $name, $price, $property);

    addProduct($product, $productId);
    header("Location:index.php");
}
function generateTypeOptions()
{
    $types = getProductTypes();
    foreach ($types as $type) {
        $typeName = $type['typeName'];
        $typeId = $type['id'];
        echo "<option value='$typeId' id='$typeName'>$typeName</option>";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Product List</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

<div class="container">
    <div class="row">
        <div class="col-8"><h1>Product add</h1></div>
        <div class="col-4" style="margin-top: 15px">
            <button form="product_form" class="btn btn-outline-primary" id="submit">Save</button>
            <a href="/" class="btn btn-outline-danger">Cancel</a></div>
    </div>
    <hr>

    <form id="product_form" action="add-product.php" method="POST">
        <table>
            <tr>
                <td><label for="sku">SKU: </label></td>
                <td><input type="text" id="sku" name="sku" required></td>
            </tr>
            <tr>
                <td><label for="name">Product name: </label></td>
                <td><input type="text" id="name" name="name" required></td>
            </tr>
            <tr>
                <td><label for="price">Price ($): </label></td>
                <td><input type="number" min="0" id="price" name="price" required></td>
            </tr>
        </table>
        <br>
        <label for="productType">Product type: </label>
        <select id="productType" name="productType" onchange="expandAttributeSelect()" required>
                <option value="">--Select type--</option>
                <?php generateTypeOptions() ?>
        </select>
        <br>

        <div id="1" class="attribute">
            <label for="weight">Weight (Kg): </label>
            <input type="number" id="weight" name="weight">
            <strong>Please enter the weight in kilograms</strong>
        </div>

        <div id="2" class="attribute">
            <div>
                <label for="height">Height (CM): </label>
                <input type="number" id="height" name="height">
            </div><div>
                <label for="width">Width (CM): </label>
                <input type="number" id="width" name="width">
            </div><div>
                <label for="length">Length (CM): </label>
                <input type="number" id="length" name="length">
            </div>
            <strong>Please enter the dimensions in a HxWxL centimeters</strong>
        </div>

        <div id="3" class="attribute">
            <label for="size">Size (MB): </label>
            <input type="number" id="size" name="size">
            <strong>Please enter the size in megabytes</strong>
        </div>

    </form>
    <hr>
</div>

</body>
</html>

<style>
    .attribute { display: none; }
</style>

<script>
    function expandAttributeSelect() {
        let selectedValue = document.getElementById("productType").value;
        let divs = document.getElementsByClassName("attribute");
            for (let i = 0; i < divs.length; i++) {
                divs[i].style.display = "none";
            }
            document.getElementById(selectedValue).style.display = "block";
        }
</script>
