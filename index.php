<?php
require_once('Services/ProductDAL.php');
if (!empty($_POST)) {
    if (isset($_POST['delete-products'])) {
        massDelete($_POST['delete-products']);
    }
}

function generateProductList()
{
    $products = getProducts();
    foreach ($products as $product) {
        $id = $product['id'];
        $SKU = $product['SKU'];
        $name = $product['name'];
        $price = $product['price'];
        $propertyName = $product['propertyName'];
        $unit = $product['unit'];
        $value = $product['value'];
        echo "<div class='col-3 item-box'>
<input type='checkbox' value='$id' class='delete-checkbox' name='delete-products[]'/>
<div class='item'>
<div>$SKU</div>
<div>$name</div>
<div>$price $</div>
<div>$propertyName: $value $unit</div>
</div>
</div>";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Product List</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

<div class="container">
    <div class="row">
        <div class="col-8"><h1>Product list</h1></div>
        <div class="col-4" style="margin-top: 15px">
            <a href="add-product.php" class="btn btn-outline-primary">Add</a>
            <button form="products" class="btn btn-outline-danger" id="delete-product-btn">Mass delete</button>
        </div>
    </div>
    <hr>
    <form method="POST" action="index.php" id="products">
        <div class="row">
            <?php generateProductList(); ?>
        </div>
    </form>

    <hr>
</div>

</body>
</html>

<style>
    .item-box {
        border: 1px solid black;
        min-height: 150px;
        max-width: 200px;
        margin: 3%;
    }
    .item {
        text-align: center;
    }
</style>
