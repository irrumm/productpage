CREATE TABLE Types (
    id int NOT NULL AUTO_INCREMENT,
    typeName varchar(255),
    PRIMARY KEY (id)
);

CREATE TABLE Properties (
    id int NOT NULL AUTO_INCREMENT,
    propertyName varchar(255),
    unit varchar(32),
    PRIMARY KEY (id)
);

CREATE TABLE Products (
    id int NOT NULL AUTO_INCREMENT,
    typeId int,
    SKU varchar(255),
    name varchar(255),
    price int,
    PRIMARY KEY (id),
    FOREIGN KEY (typeId) REFERENCES Types(id)
);

CREATE TABLE Attributes (
    id int NOT NULL AUTO_INCREMENT,
    productId int,
    propertyId int,
    value varchar(255),
    PRIMARY KEY (id),
    FOREIGN KEY (productId) REFERENCES Products(id),
    FOREIGN KEY (propertyId) REFERENCES Properties(id)
);
