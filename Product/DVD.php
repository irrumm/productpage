<?php

require_once "BaseProduct.php";

class DVD extends BaseProduct {
    private $size;

    public function __construct($SKU, $name, $price, $size) {
        parent::__construct($SKU, $name, $price);
        $this->size = $size;
    }

    public function getSize() {
        return $this->size;
    }

    public function setSize($size)
    {
        $this->size = $size;
    }

    public function getAttributeType()
    {
        return "Size";
    }

    public function getAttributeUnit()
    {
        return "MB";
    }

    public function getAttributeValue()
    {
        return $this->getSize();
    }

    public function setAttributeValue($value)
    {
        $this->setSize($value);
    }
}