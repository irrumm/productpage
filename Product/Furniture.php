<?php

require_once "BaseProduct.php";

class Furniture extends BaseProduct {
    private $dimensions;

    public function __construct($SKU, $name, $price, $dimensions) {
        parent::__construct($SKU, $name, $price);
        $this->dimensions = $dimensions;
    }

    public function getDimensions() {
        return $this->dimensions;
    }

    public function setDimensions($dimensions)
    {
        $this->dimensions = $dimensions;
    }

    public function getAttributeType()
    {
        return "Dimensions";
    }

    public function getAttributeUnit()
    {
        return "CM";
    }

    public function getAttributeValue()
    {
        return $this->getDimensions();
    }

    public function setAttributeValue($value)
    {
        $this->setDimensions($value);
    }
}