<?php

require_once "BaseProduct.php";

class Book extends BaseProduct {
    private $weight;

    public function __construct($SKU, $name, $price, $weight) {
        parent::__construct($SKU, $name, $price);
        $this->weight = $weight;
    }

    public function getWeight() {
        return $this->weight;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function getAttributeType()
    {
        return "Weight";
    }

    public function getAttributeUnit()
    {
        return "Kg";
    }

    public function getAttributeValue()
    {
        return $this->getWeight();
    }

    public function setAttributeValue($value)
    {
        $this->setWeight($value);
    }
}