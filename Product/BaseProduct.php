<?php

abstract class BaseProduct {

    protected $SKU;
    protected $name;
    protected $price;

    public function __construct($SKU, $name, $price) {
        $this->SKU = $SKU;
        $this->name = $name;
        $this->price = $price;
    }

    public function getSKU() {
        return $this->SKU;
    }

    public function setSKU($SKU) {
        $this->SKU = $SKU;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getPrice() {
        return $this->price;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public function getAttributeType(){ return null; }
    public function getAttributeUnit(){ return null; }
    public function getAttributeValue(){ return null; }
    public function setAttributeValue($value) { return null; }
}