<?php

require_once ("config.php");
require_once ("PropertyDAL.php");
require_once ("AttributeDAL.php");

function addProduct(BaseProduct $product, $typeId) {
    $con = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_DB);

    if ($con-> connect_errno) {
        trigger_error('Database connection failed: ' . $con->connect_error);
    }

    $SKU = $product->getSKU();
    $name = $product->getName();
    $price = $product->getPrice();
    $value = $product->getAttributeValue();

    $property = getPropertyByName($product->getAttributeType(), $product->getAttributeUnit());

    $stmt = $con->prepare(
        "INSERT INTO `Products` (typeId, SKU, `name`, price) VALUES (?,?,?,?);"
    );
    $stmt->bind_param('ssss', $typeId, $SKU, $name, $price);
    $stmt->execute();

    $productId = $con->insert_id;

    addAttribute($productId, $property['id'], $value);
}

function getProducts() {
    $con = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_DB);

    if ($con-> connect_errno) {
        trigger_error('Database connection failed: ' . $con->connect_error);
    }

    $products = [];
    $query = "SELECT p.id, p.SKU, p.name, p.price, a.value, prop.propertyName, prop.unit, t.typeName FROM Products p left join Attributes a on p.id = a.productId left join Properties prop on a.propertyId = prop.id left join Types t on p.typeId = t.id";
    $result = $con->query($query);

    while($row = mysqli_fetch_assoc($result)){
        $products[] = $row;
    }
    return $products;
}

function massDelete($productIds) {
    $ids = implode("','", $productIds);

    $con = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_DB);

    if ($con-> connect_errno) {
        trigger_error('Database connection failed: ' . $con->connect_error);
    }

    $stmt = $con->prepare("DELETE FROM `Attributes` WHERE productId IN ('".$ids."')");
    $stmt->execute();

    $stmt = $con->prepare("DELETE FROM `Products` WHERE id IN ('".$ids."')");
    $stmt->execute();
    $con->close();
}