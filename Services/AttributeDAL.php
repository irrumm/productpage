<?php

require_once ("config.php");

function addAttribute($productId, $propertyId, $value) {
    $con = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_DB);

    if ($con-> connect_errno) {
        trigger_error('Database connection failed: ' . $con->connect_error);
    }

    $stmt = $con->prepare(
        'INSERT INTO `Attributes` (productId, propertyId, value) VALUES (?, ?, ?);'
    );
    $stmt->bind_param('dds', $productId, $propertyId, $value);
    $stmt->execute();

    $con->close();
}
