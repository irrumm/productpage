<?php

require_once ("config.php");

function getProductTypes() {
    $con = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_DB);

    if ($con-> connect_errno) {
        trigger_error('Database connection failed: ' . $con->connect_error);
    }

    $types = [];
    $query = "SELECT * FROM `Types`";
    $result = $con->query($query);

    while($row = mysqli_fetch_assoc($result)){
        $types[] = $row;
    }
    return $types;
}

function getTypeById($typeId) {
    $con = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_DB);

    if ($con-> connect_errno) {
        trigger_error('Database connection failed: ' . $con->connect_error);
    }

    $query = "SELECT * FROM Types WHERE id = '$typeId'";
    $result = $con->query($query);

    return mysqli_fetch_assoc($result);
}